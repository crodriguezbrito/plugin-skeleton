<?php

/*
 * Plugin Name: YITH Plugin Skeleton
 * Description: Skeleton for YITH Plugins
 * Version: 1.0.0
 * Author: Carlos Rodríguez 
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-skeleton
 */

! defined( 'ABSPATH' ) && exit;   //Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_PS_VERSION' ) ) {
	define( 'YITH_PS_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PS_DIR_URL' ) ) {
	define( 'YITH_PS_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PS_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PS_DIR_ASSETS_URL', YITH_PS_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PS_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PS_DIR_ASSETS_CSS_URL', YITH_PS_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PS_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PS_DIR_ASSETS_JS_URL', YITH_PS_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PS_DIR_PATH' ) ) {
	define( 'YITH_PS_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PS_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PS_DIR_INCLUDES_PATH', YITH_PS_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PS_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PS_DIR_TEMPLATES_PATH', YITH_PS_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PS_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PS_DIR_VIEWS_PATH', YITH_PS_DIR_PATH . 'views' );
}

// Different way to declare a constant

! defined( 'YITH_PS_INIT' )               && define( 'YITH_PS_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PS_SLUG' )               && define( 'YITH_PS_SLUG', 'yith-plugin-skeleton' );
! defined( 'YITH_PS_SECRETKEY' )          && define( 'YITH_PS_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );
! defined( 'YITH_PS_OPTIONS_PATH' )       && define( 'YITH_PS_OPTIONS_PATH', YITH_PS_DIR_PATH . 'plugin-options' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_ps_init_classes' ) ) {

	function yith_ps_init_classes() {

		load_plugin_textdomain( 'yith-plugin-skeleton', false, basename( dirname( __FILE__ ) ) . '/languages' );

		//Require all the files you include on your plugins. Example
		require_once YITH_PS_DIR_INCLUDES_PATH . '/class-yith-ps-plugin-skeleton.php';

		if ( class_exists( 'YITH_PS_Plugin_Skeleton' ) ) {
			/*
			*	Call the main function
			*/
			yith_ps_plugin_skeleton();
		}
	}
}


add_action( 'plugins_loaded', 'yith_ps_init_classes', 11 );




